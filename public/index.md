---
pagetitle: Vinculación Inclusiva 2019
---

#  6° Convocatoria a Proyectos de Vinculación Tecnológica y Desarrollo Productivo de la UNR. VINCULACIÓN INCLUSIVA 2019

## Fechas importantes

El **cierre** de la convocatoria será el **lunes 27 de mayo** para presentación
del Anexo 1 y **viernes 31 de mayo** para presentación del Anexo 2 (Avales).

## Línea Iniciación

10 proyectos de $ 75.000 c/u.

## Bases y Condiciones y anexos

[Bases y Condiciones](Bases_y_Condiciones.pdf)

[ANEXO N1](ANEXO_N_1_2019.doc)

[ANEXO_N2](ANEXO_N_2_2019.doc)

## 3- Líneas de Financiamiento
### Ejes Línea Iniciación

1. Apoyo a emprendimientos productivos y sociales
Apunta a impulsar la formación de emprendedores, la generación de nuevas empresas y toda iniciativa que promueva la creación de empleo mediante el impulso y articulación de los sectores académico y productivo.
3. Innovación social Elaboración de acciones conjuntas que brinden soluciones a una demanda social existente (discapacidad, hábitat y vivienda, salud, energías alternativas, transporte, etc.).

### ¿Con quiénes deben trabajar?
Se valorará la vinculación con pequeñas y medianas empresas (PyMEs), cooperativas, asociaciones empresarias, sindicales o profesionales, y empresas de la economía social; municipios, comunas o consorcio de municipios y agencias de desarrollo; instituciones públicas y privadas, como hospitales o escuelas, dependencias estatales sectoriales, y **organizaciones de la Sociedad Civil.**

# Plazo de ejecución

Duración máxima de un (1) año, con posibilidad de prórroga de hasta seis (6) meses más.

# Objetivo principal de “Vinculación Inclusiva”

“Vinculación Inclusiva” tiene como objetivo principal vincular a la Universidad, a través de todos sus estamentos, con los actores locales involucrados en procesos productivos, generando respuestas y estrechando lazos de confianza y cooperación, promoviendo de esta forma una cultura innovadora y fortaleciendo el desarrollo territorial de la región.

Los proyectos de Vinculación Tecnológica buscan así valorizar el conocimiento científico y comunitario, reconociendo demandas sociales existentes o latentes, formulando un conjunto de actividades coherentes con los objetivos planteados y movilizando recursos para su resolución.

# Principios básicos

- Vinculación y asociatividad con empresas, municipios y comunas, e instituciones públicas y privadas del medio productivo local.
- Generación de procesos de aprendizaje, integrando el conocimiento basado en la ciencia, con aquél basado en la práctica y la experiencia.
- Co-construcción de conocimiento que acompañe los procesos de cambio del sector productivo, priorizando las necesidades territoriales y la sostenibilidad en el tiempo.

# Desarrollo de la propuesta (borrador 05-05-19)

##  Red Comunitaria de acceso a Internet

> “La única y cambiante naturaleza de internet no sólo permite a los individuos ejercer su derecho de opinión y expresión, sino que también forma parte de sus derechos humanos y promueve el progreso de la sociedad en su conjunto” Frank La Rue, Relator Especial de ONU.

**El proyecto propone desarrollar una Red Comunitaria que brinde acceso a Internet a la unidad productiva de la facultad y a los pequeños productores de la zona.** Esto permitirá potenciar el trabajo que se viene desarrollando - por más de 10 años - desde el Centro de Información de Actividades Porcinas (CIAP), cuya misión es *colaborar con información, conocimientos y vinculaciones de importancia mediante el uso de TIC y esfuerzos cooperativos.*

> Las Redes Comunitarias son redes inalámbricas descentralizadas, desplegadas con routers WiFi, un firmware que permite modificar su funcionamiento y equipos diseñados específicamente como es el caso de LibreRouter, redes que luego mantienen las propias comunidades. Se caracterizan porque las pueden administrar personas sin conocimientos previos de redes e informática.

> Una red comunitaria es de propiedad y gestión colectiva de la comunidad, sin finalidad de lucro y con fines comunitarios.

La red servirá también como plataforma para desplegar sensores - *desarrollados con tecnologías libres* - que permitan monitorear distintas variables ambientales en las instalaciones de los productores. Además, permitirá generar tecnología propia para estas mediciones, dejando de utilizar instrumentos ad hoc que tienen precios elevados y son equipos cerrados (no se puede agregar nuevas funcionalidades).

Las redes libres y comunitarias tienen el rol fundamental de facilitar la construcción de infraestructuras tecnológicas autónomas de bajo costo para desplegar redes de Internet. El objetivo principal es vencer las barreras que imponen la centralización y el control de las infraestructuras de las redes comerciales y de los contenidos que por ellas circulan.

Como sucedió con cada nueva tecnología de comunicación, el desarrollo de Internet se articula con movimientos cooperativos, comunitarios o de “abajo hacia arriba” que buscan modificar la lógica mercantil y de consumo que ha adquirido con su masificación (como sucedió antes con el teléfono, la radio y la televisión, por ejemplo). Luego de una “primera ola” de redes comunitarias que aparecieron a partir de la década de 1990 en muchos centros urbanos del mundo (y algunas experiencias locales), en la actualidad estos proyectos apuntan a brindar acceso a Internet en zonas rurales y a sectores en situación de vulnerabilidad, donde el sector privado no obtiene retorno de inversión y las políticas públicas tampoco han dado respuestas.

AlterMundi - asociación civil sin fines de lucro - ha generado el despliegue de Redes Comunitarias en todo el mundo. Además impulsó la creación de LibreRouter, un router diseñado desde su concepción para las Redes Comunitarias y coordina el proyecto LibreMesh, un firmware que se puede instalar en algunos modelos de routers convencionales.

En este proyecto se trabajará según el modelo propuesto por AlterMundi:

> "El principio rector en nuestro caso es lograr el máximo resultado con un mínimo de recursos. Este modelo se centra en un bajo costo económico y una escasa complejidad de instalación y operación, que posibilitan su alta versatilidad y facilidad de despliegue"

#### Referencias

CIAP: [http://www.ciap.org.ar](http://www.ciap.org.ar)
AlterMundi: [https://altermundi.net](https://altermundi.net)

LibreRouter: [https://librerouter.org](https://librerouter.org)

LibreMesh: [https://libremesh.org](https://libremesh.org)
